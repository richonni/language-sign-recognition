from PIL import Image
import numpy as np
import sys
import os
import csv
#from skimage.transform import resize
import matplotlib.pyplot as plt
from PIL import Image

csv_file = "ArASL_data64.csv"

#Useful function

def eclatechemin(chemin):
    """éclate un chemin Windows et renvoie la liste de ses éléments"""
    drive, chemin = os.path.splitdrive(chemin)
    L = [drive]
    while chemin not in [u'', u'\\', u'/']:
        chemin, elem = os.path.split(chemin)
        L.insert(1, elem)
    return L

def createFileList(myDir, format='.jpg'):
    fileList = []
    print(myDir)
    for root, dirs, files in os.walk(myDir, topdown=False):
        for name in files:
            if name.endswith(format):
                fullName = os.path.join(root, name)
                fileList.append(fullName)
    return fileList


# load the original image
fileList = createFileList('/Users/loic/Documents/Eurecom/Malis/malis_project/ArASL_Database_54K_Final')

value = ["label"]
for i in range(1,4097):
    value.append("pixel"+str(i))
value= np.array(value)
    

with open(csv_file, 'a') as f:
        writer = csv.writer(f)
        writer.writerow(value)


for file in fileList:
    y = np.array([eclatechemin(file)[8]])
    img_file = Image.open(file)
    #print(img_file)
    img_file = img_file.resize((64,64))
    #print(img_file)
    #img_file = cv2.resize(img_file, dsize=(28, 28), interpolation=cv2.INTER_CUBIC)
    #img_file = resize(img_file, (28, 28))
    #img_file.show()
    
    # get original image parameters...
    width, height = img_file.size
    format = img_file.format
    mode = img_file.mode

    # Make image Greyscale
    img_grey = img_file.convert('L')
    #img_grey.save('result.png')
    #img_grey.show()

    # Save Greyscale values
    value = np.asarray(img_grey.getdata(), dtype=np.int).reshape((img_grey.size[1], img_grey.size[0]))
   
    
    value = value.flatten()
    value = np.concatenate((y,value))
   
    with open(csv_file, 'a') as f:
        writer = csv.writer(f)
        writer.writerow(value)

